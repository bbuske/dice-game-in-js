# Dice Game in JS

A simple dice game coded in HTML5, CSS3 and JavaScript 6

## Usage
This is just a small dice game, made with HTML, CSS and JS. It can be controlled by mouse.

## Support
Use the Issue Tracker, to report any ideas, suggestions, bugs or questions. 

## Contributing
Feel free to improve or modify, as long as you mention the original author. If you want to have your changes added, push them
and file a merge request.

## Authors and acknowledgment
Benjamin Buske | Buske IT

## License
GPU General Public License 2.0

## Project status
Finished. No further development. Bug Fixes and Security Patches only.